# Catalejo Luthier

The Live Hacking Music

![Esquema general](img/intro/sistemaGeneral-sistemaGeneral.svg){: style="width:100%"}

Se desea desarrollar una arquitectura donde el servidor pueda controlar tanto
la información como la comunicación entre los clientes

![Esquema abstracto](img/intro/sistemaGeneral-servidor-clientes.svg){: style="width:50%; margin-left: auto; margin-right: auto; display: block"}
<div align="center">Arquitectura General</div>


