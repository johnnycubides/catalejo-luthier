# Diseño de Hardware

## servidor

### Requerimientos

![servidor](img/hwdesing/sistemaGeneral-servidor.svg){: style="width:80%; margin-left: auto; margin-right: auto; display: block"}
<div align="center">Servidor y elementos requeridos</div>

### Propuesta de diseño

![implementación servidor](img/hwdesing/implementation-hardware-server.svg){: style="width:100%; margin-left: auto; margin-right: auto; display: block"}
<div align="center">Propuesta de implementación para el servidor</div>

### Propuesta de implementación 

#### Onion Omega2+

![Propuesta de implementación con onion omega2+](img/hwdesing/implementation-onion.svg){: style="width:100%; margin-left: auto; margin-right: auto; display: block"}

[Implementación con placa Omega2+](implemen/omega2plus.md)

## Cliente Humano

### Requerimiento

![cliente-humano](img/hwdesing/sistemaGeneral-cliente-humano.svg){: style="width:80%; margin-left: auto; margin-right: auto; display: block"}
<div align="center">Cliente y elementos requeridos</div>

### Propuesta de diseño

Los requerimientos de hardware son cubiertos por los siguientes dispositivos:

* Celular (Android, Iphone)
* Tablets (Android, Iphone)
* Computadores portátiles
* Computadores de escritorio

Por tanto, **no** es requerido diseñar un aparato para el cliente humano, pero desde el punto de vista del
software si se requiere tener en cuenta asuntos como integración e **interoperabilidad** con el servidor y los
clientes máquina.

## Cliente Máquina

### Requerimientos

![cliente-maquina](img/hwdesing/sistemaGeneral-cliente-maquina.svg){: style="width:80%; margin-left: auto; margin-right: auto; display: block"}
<div align="center">Cliente máquina y elementos requeridos</div>

### Propuesta de diseño

![implementación cliente-maquina](img/hwdesing/implementation-hardware-machine-client.svg){: style="width:100%; margin-left: auto; margin-right: auto; display: block"}
<div align="center">Propuesta de implementación para el cliente máquina</div>

### Propuesta de implementación 1 

TODO

Esp32

### Propuesta de implementación 2 

TODO

Esp8266
