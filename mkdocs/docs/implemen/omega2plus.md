# Implementación de Onion Omega2+


![Propuesta de implementación con onion omega2+](../img/hwdesing/implementation-onion.svg){: style="width:100%; margin-left: auto; margin-right: auto; display: block"}

## Módulos de Hardware

|Modulo|Caracteristica|link|
|:-------------:|:-------------:|:-----:|
|Si4735|i2s a transmisor FM| [Datasheet](https://www.silabs.com/documents/public/data-sheets/Si4730-31-34-35-D60.pdf)|
|KT0803L|Audio análogo a Transmisor FM | [datasheet](https://datasheetspdf.com/pdf-file/791333/KTMicro/KT0803L/1)|



